# CMake file for the Corryvreckan framework
CMAKE_MINIMUM_REQUIRED(VERSION 3.4.3 FATAL_ERROR)
IF(COMMAND CMAKE_POLICY)
  CMAKE_POLICY(SET CMP0003 NEW) # change linker path search behaviour
  CMAKE_POLICY(SET CMP0048 NEW) # set project version
ENDIF(COMMAND CMAKE_POLICY)

PROJECT(BoolStringTest VERSION 1.0 LANGUAGES CXX)

SET(CMAKE_INSTALL_PREFIX "${PROJECT_SOURCE_DIR}" CACHE PATH "Prefix prepended to install directories" FORCE)

SET(CMAKE_CXX_STANDARD 17)

# create executable and link the libs
ADD_EXECUTABLE(main main.cpp text.cpp)

# set install location
INSTALL(TARGETS main EXPORT main_install
  RUNTIME DESTINATION bin
  ARCHIVE DESTINATION lib)

